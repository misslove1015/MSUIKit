//
//  UIView+MSFrame.h
//  App
//
//  Created by miss on 2017/10/21.
//  Copyright © 2017年 miss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MSFrame)

@property (nonatomic, assign) CGFloat left, right, top, bottom;
@property (nonatomic, assign) CGFloat x, y, width, height;
@property (nonatomic, assign) CGSize  size;
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGPoint boundsCenter;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;

@end
