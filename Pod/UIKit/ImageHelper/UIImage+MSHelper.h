//
//  UIImage+MSHelper.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MSHelper)

// 压缩图片至指定大小
- (UIImage *)ms_compressImageToSize:(CGSize)targetSize;

@end
