//
//  UIButton+MSHelper.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

// 按钮扩展，包括设置网络图片和添加点击事件

#import <UIKit/UIKit.h>

@interface UIButton (MSHelper)

// 设置背景网络图片，并自动设置占位图片
- (void)ms_setBackgroundImageWithURL:(NSString *)url;

// 设置背景网络图片，使用自定义占位图片
- (void)ms_setBackgroundImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage;

// 设置网络图片，并自动设置占位图片
- (void)ms_setImageWithURL:(NSString *)url;

// 设置网络图片，使用自定义占位图片
- (void)ms_setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage;

// 添加按钮点击事件
- (void)ms_addTouchUpInsideEvent:(void(^)(UIButton *button))actionBlock;

@end
