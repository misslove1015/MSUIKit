//
//  UIButton+MSHelper.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "UIButton+MSHelper.h"
#import <objc/runtime.h>
#import <SDWebImage/UIButton+WebCache.h>

static char *actionBlockKey;

@implementation UIButton (MSHelper)

- (void)ms_setBackgroundImageWithURL:(NSString *)url {
    [self sd_setBackgroundImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:nil];
}

- (void)ms_setBackgroundImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage {
    [self sd_setBackgroundImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:placeholderImage];
}

- (void)ms_setImageWithURL:(NSString *)url {
    [self sd_setImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:nil];
}

- (void)ms_setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage {
    [self sd_setImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal placeholderImage:placeholderImage];
}

- (void)ms_addTouchUpInsideEvent:(void(^)(UIButton *button))actionBlock {
    objc_setAssociatedObject(self, &actionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonClick:(UIButton *)button {
    void (^actionBlock) (UIButton *button) = objc_getAssociatedObject(self, &actionBlockKey);
    if (actionBlock) {
        actionBlock(button);
    }
}

@end
