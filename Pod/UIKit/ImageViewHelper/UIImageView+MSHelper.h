//
//  UIImageView+MSHelper.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (MSHelper)

// 设置网络图片，并自动设置占位图片
- (void)ms_setWebImageWithURL:(NSString *)url;

// 设置网络图片，使用自定义的占位图片
- (void)ms_setWebImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage;

@end
