//
//  UIImageView+MSHelper.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "UIImageView+MSHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UIImageView (MSHelper)

// 设置网络图片，并自动设置占位图片
- (void)ms_setWebImageWithURL:(NSString *)url {
    [self sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil];
}

// 设置网络图片，使用自定义的占位图片
- (void)ms_setWebImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholderImage {
    [self sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholderImage];
}

@end
