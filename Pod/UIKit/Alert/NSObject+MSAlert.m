//
//  NSObject+MSAlert.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "NSObject+MSAlert.h"

@implementation NSObject (MSAlert)

- (void)alertWithTitle:(NSString*)title
               message:(NSString*)message {
    [self alertWithTitle:title message:message confirmTitle:@"确定" cancelTitle:@"取消" buttonAction:nil];
}

- (void)alertWithTitle:(NSString*)title
               message:(NSString*)message
   confirmButtonAction:(void(^)(void))confirmAction {
    [self alertWithTitle:title message:message confirmTitle:@"确定" cancelTitle:@"取消" buttonAction:^(NSInteger index) {
        if (index == 1) {
            if (confirmAction) {
                confirmAction();
            }
        }
    }];
}

- (void)alertWithTitle:(NSString*)title
               message:(NSString*)message
          confirmTitle:(NSString *)confirmTitle
           cancelTitle:(NSString *)cancelTitle
          buttonAction:(void(^)(NSInteger index))buttonAction {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    if (cancelTitle) {
        [alert addAction:[UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (buttonAction) {
                buttonAction(0);
            }
        }]];
    }
    if (confirmTitle) {
        [alert addAction:[UIAlertAction actionWithTitle:confirmTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action) {
            if (buttonAction) {
                buttonAction(1);
            }
        }]];
    }
    
    [self presentAlertController:alert];
}

- (void)presentAlertController:(UIViewController*)controller {
    UIViewController* rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [rootViewController presentViewController:controller animated:YES completion:nil];
}

@end
