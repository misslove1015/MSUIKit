//
//  NSObject+MSAlert.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

// 弹框

#import <Foundation/Foundation.h>

@interface NSObject (MSAlert)

// 弹框，没有按钮事件
- (void)alertWithTitle:(NSString*)title
               message:(NSString*)message;

// 弹框，包含确定按钮事件
- (void)alertWithTitle:(NSString*)title
               message:(NSString*)message
   confirmButtonAction:(void(^)(void))confirmAction;

// 弹框，包含确定和取消按钮事件
- (void)alertWithTitle:(NSString*)title
               message:(NSString*)message
          confirmTitle:(NSString *)confirmTitle
           cancelTitle:(NSString *)cancelTitle
          buttonAction:(void(^)(NSInteger index))buttonAction;

@end
