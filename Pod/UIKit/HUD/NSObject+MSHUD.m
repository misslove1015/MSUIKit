//
//  NSObject+MSHUD.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "NSObject+MSHUD.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation NSObject (MSHUD)

- (void)showToast:(NSString *)text {
    [self showToast:text toView:[UIApplication sharedApplication].delegate.window];
}

- (void)showToast:(NSString *)text toView:(UIView *)view {
    MBProgressHUD *textHUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    textHUD.mode = MBProgressHUDModeText;
    textHUD.label.text = text;
    textHUD.label.textColor = [UIColor whiteColor];
    textHUD.margin = 10;
    textHUD.bezelView.color = [UIColor blackColor];
    textHUD.userInteractionEnabled = NO;
    [textHUD showAnimated:YES];
    [textHUD hideAnimated:YES afterDelay:1];
}

// 显示 Loading，默认显示在 Window 上
- (void)showLoadingHUD {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window animated:YES];
    hud.userInteractionEnabled = NO;
}

// 隐藏 Loading
- (void)hideLoadingHUD {
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].delegate.window animated:YES];
}

// 显示 Loading 在某个 View 上
- (void)showLoadingHUDToView:(UIView *)view {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.userInteractionEnabled = NO;
}

// 隐藏 某个 View 的Loading
- (void)hideLoadingHUDWithView:(UIView *)view {
    [MBProgressHUD hideHUDForView:view animated:YES];
}
@end
