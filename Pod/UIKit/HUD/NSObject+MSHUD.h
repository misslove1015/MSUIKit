//
//  NSObject+MSHUD.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

// HUD，包括 Toast 和 Loading

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSObject (MSHUD)

// 显示 Toast，默认显示在 Window 上
- (void)showToast:(NSString *)text;

// 显示 Toast 在某个 View 上
- (void)showToast:(NSString *)text toView:(UIView *)view;

// 显示 Loading，默认显示在 Window 上
- (void)showLoadingHUD;

// 隐藏 Loading
- (void)hideLoadingHUD;

// 显示 Loading 在某个 View 上
- (void)showLoadingHUDToView:(UIView *)view;

// 隐藏 某个 View 的 Loading
- (void)hideLoadingHUDWithView:(UIView *)view;

@end
