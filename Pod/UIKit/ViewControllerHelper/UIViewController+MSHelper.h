//
//  UIViewController+MSHelper.h
//  MSUIKit
//
//  Created by 郭明亮 on 2018/9/17.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MSHelper)

// 是否是我们自己的 ViewController
- (BOOL)isOurVC;

@end
