//
//  UIViewController+MSHelper.m
//  MSUIKit
//
//  Created by 郭明亮 on 2018/9/17.
//

#import "UIViewController+MSHelper.h"
#import <objc/runtime.h>
#import <Aspects/Aspects.h>

@implementation UIViewController (MSHelper)

+ (void)load {
    [UIViewController aspect_hookSelector:@selector(viewDidLoad) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> info){
        UIViewController *vc = (UIViewController *)info.instance;
        if ([vc isOurVC]) {
            vc.view.backgroundColor = [UIColor yellowColor];
        }
    } error:nil];
    
    [UIViewController aspect_hookSelector:NSSelectorFromString(@"dealloc") withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo> info){
        UIViewController *vc = (UIViewController *)info.instance;
        if ([vc isOurVC]) {
            NSLog(@"%@ dealloc", NSStringFromClass(vc.class));
        }
    } error:nil];
}

// UI _UI：系统，CAM：拍照库 ，PUUI：相册库，PLPhoto：相册编辑，CKSMS：短信
- (BOOL)isOurVC {
    NSString *selfClass = NSStringFromClass([self class]);
    if([selfClass hasPrefix:@"UI"] ||
       [selfClass hasPrefix:@"_UI"]  ||
       [selfClass hasPrefix:@"CKSMS"] ||
       [selfClass hasPrefix:@"CAM"] ||
       [selfClass hasPrefix:@"PUUI"] ||
       [selfClass hasPrefix:@"PLPhoto"] ||
       [self isKindOfClass:[UINavigationController class]] ||
       [self isKindOfClass:[UITabBarController class]]){
        return NO;
    }
    return YES;
}

@end
