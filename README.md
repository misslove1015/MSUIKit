## MSUIKit

### 介绍    
UIKit 基础库，包含 UIKit 相关扩展

### 使用方法  
1.Podfile 添加下面代码，执行 pod install   

	 ms_pod "MSUIKit", :tag => "v1.0", :git => "git@gitlab.com:misslove1015/MSUIKit.git", :inhibit_warnings => false , :ms_debug => $msDebugSwitch, :ms_branch => $msBranch, :ms_useRemote => $msUseRemote
    
2.引入头文件
 
    #import <MSUIKit/MSUIKit.h>
   

