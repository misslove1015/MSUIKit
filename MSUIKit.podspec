
Pod::Spec.new do |s|
  s.name             = "MSUIKit"
  s.version          = "0.1.0"
  s.summary          = "MSUIKit"
  s.description      = <<-DESC.gsub(/^\s*\|?/,'')
                       An optional longer description of XUEZHIYUN

                       | * Markdown format.
                       | * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "https://gitlab.com/misslove1015/MSUIKit.git"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'BSD'
  s.author           = { "miss" => "13116760713@163.com" }
  s.source           = { :git => "https://gitlab.com/misslove1015/MSUIKit.git", :branch => "master" }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.public_header_files = 'Pod/**/*.h'
  s.source_files = 'Pod/**/*.{h,m}'

   s.resource_bundles = {
    'MSUIKit' => ['Pod/Assets/*.png','Pod/Assets/*.jpg']
  }

  # Uncomment following lines if needs to link with some static libraries.
  # s.vendored_libraries = 'Pod/Lib/*.a'
  # s.vendored_frameworks = 'Pod/Frameworks/*.framework'


s.prefix_header_contents = <<-EOS
#ifdef __OBJC__

#endif /* __OBJC__*/
EOS

# Uncomment following lines if ZHUserKit depends on any system framework.
# s.frameworks = 'UIKit'

# s.weak_framework = 'MessageUI'

# Uncomment following lines if ZHUserKit depends on any public or private pod.
s.dependency 'MBProgressHUD'
s.dependency 'SDWebImage'
s.dependency 'MSFoundation'
s.dependency 'Aspects'

  
end
